/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import configureStore from './src/redux';


import {createStackNavigator} from '@react-navigation/stack';
import Contact from './src/screens/Contact';
import Chat from './src/screens/Chat';

const StackNavigator = createStackNavigator(); 

const reduxStore = configureStore();

const App = () => {
  return (
    <Provider store={reduxStore}>
       <SafeAreaProvider
       >
        <NavigationContainer>
          <StackNavigator.Navigator initialRouteName="Contact" screenOptions={{
            headerShown: false,
          }}>
            <StackNavigator.Screen name="Contact" component={Contact} />
            <StackNavigator.Screen name="Chat" component={Chat} />
          </StackNavigator.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
    </Provider>
  );
};

export default App;
