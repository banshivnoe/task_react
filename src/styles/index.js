import {StyleSheet, Platform, StatusBar} from 'react-native';
import {colors} from '../utils/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.themeColor,
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  rowHeader: {
    flexDirection: 'row',
    width: wp('100%'),
    height: hp('7.2%'),
    marginTop: wp('5%'),
  },
  rowSearch: {
    flexDirection: 'row',
    width: wp('100%'),
    height: hp('8%'),
    justifyContent: 'center',

  },
  rowUsers: {
    flexDirection: 'row',
    width: wp('95%'),
    height: hp('29%'),
    marginLeft: wp('2.5%'),
    marginRight: wp('2.5%'),
  },
  rowChatList: {
    flexDirection: 'row',
    width: wp('100%'),
    height: hp('58%'),
    paddingTop:hp('1%')
  },
  chatRowContainer: {
    height: hp('7%'),
    flexDirection: 'row',
  },
  ImageProfleContainer: {
    flexDirection: 'column',
    width: wp('20%'),
    height: hp('9%'),
  },
  profilePic: {
    position: 'absolute',
    left: wp('10.6%'),
    top: hp('3.3%'),
    width: hp('4.18%'),
    height: hp('4.18%'),
    borderRadius: hp('2.9%'),
    overflow: 'hidden',
  },
  profilePicWithoutCount: {
    width: hp('5.4%'),
    marginLeft: wp('5.6%'),
    marginTop: hp('1.23%'),
    height: hp('5.4%'),
    borderRadius: hp('2.7%'),
    overflow: 'hidden',
  },
  profilePicWithoutCount: {
    width: hp('5.4%'),
    marginLeft: wp('5.6%'),
    marginTop: hp('1.23%'),
    height: hp('5.4%'),
    borderRadius: hp('2.7%'),
    overflow: 'hidden',
  },
 
  viewCountContainer: {
    position: 'absolute',
    left: wp('5.33%'),
    top: hp('1.23%'),
    width: hp('4.4%'),
    height: hp('4.4%'),
    borderRadius: hp('2.2%'),
    backgroundColor: 'rgba(65, 71, 86, 1)',
    justifyContent: 'center',
    elevation: 5,
  },
  textCount: {
    color: 'white',
    fontSize: hp('2%'),
    fontWeight: '400',
    lineHeight: hp('2.4%'),
    textAlign: 'center',
  },
  userMessageContainer: {
    flexDirection: 'column',
    width: wp('58%'),
    marginLeft: wp('4%'),
  },
  userNameText: {
    color: 'rgba(255, 255, 255, 1)',
    fontSize: hp('2%'),
    fontWeight: '400',
    lineHeight: hp('2.4%'),
    textAlign: 'left',
    paddingTop: wp('2%'),
    paddingRight: wp('2%'),
  },
  userMessageText: {
    color: 'rgba(255, 255, 255, 1)',
    fontSize: hp('1.7%'),
    fontWeight: '300',
    lineHeight: hp('1.8%'),
    textAlign: 'left',
    paddingTop: wp('1.5%'),
    paddingBottom: wp('2%'),
  },
  dateTimeComtainer: {
    flexDirection: 'column',
    width: wp('22%'),
  },
  dateTimeText: {
    color: 'white',
    fontSize: hp('2%'),
    fontWeight: '400',
    lineHeight: hp('2.4%'),
    textAlign: 'right',
    paddingTop: wp('2%'),
    paddingRight: wp('6%'),
  },
  lineSeprator: {
    height: hp('1.35%'),
  },
  headerImage:{flexDirection: 'column', width: wp('20%'),justifyContent:'center'},
  headerNameCont:{
    flexDirection: 'column',
    width: wp('80%'),
    justifyContent: 'center',
  },
  userNameTitle:{
    fontWeight: '400',
    fontSize: wp('7.2%'),
    lineHeight: wp('8.3%'),
    color: '#FFFFFF',
    textAlign: 'left',
    paddingLeft:wp('1.2%'),
    paddingTop:wp('1.2%')
  },
  searchContainer:{
    flexDirection: 'column',
    width: wp('80%'),
    justifyContent: 'center',
  },
  searchView:{
    backgroundColor: 'rgba(0, 0, 0, 0.25)',
    borderRadius: wp('1.8%'),
    width: wp('80%'),
    height: wp('10%'),
    marginLeft: wp('7%'),
    flexDirection: 'row',
    justifyContent: 'center',
  
  },
  searchBarText:{width: wp('80%'), height: wp('10%'),color:'white',fontSize:wp('3.7%'),paddingLeft:wp('4%')},
  searchBarImage:{
    flexDirection: 'column',
    position: 'absolute',
    right: 0,
    justifyContent: 'center'
  },
searchBarIconBack:{
  backgroundColor: 'rgba(86, 94, 112, 1)',
  height: wp('10%'),
  width: wp('10%'),
  borderRadius: wp('1.8%'),
  justifyContent: 'center',
  alignItems: 'center',
},
searchBarIcon: {
  width: wp('5.7%'),
  height: wp('5.7%'),
  overflow: 'hidden',
},

buttonPlusContainer:{
  flexDirection: 'column',
  width: wp('20%'),
  justifyContent: 'center',
},
buttonPlusIcon:{
  backgroundColor: 'rgba(3, 169, 241, 1)',
  height: wp('8.5%'),
  width: wp('8.5%'),
  borderRadius: wp('1%'),
  position: 'absolute',
  right: wp('2%'),
  justifyContent: 'center',
  alignItems: 'center',
},
textPlus:
{
  fontSize: wp('5%'),
  color: 'rgba(255, 255, 255, 1)',
  fontWeight: '400',
},
textFav:
  {
    fontSize: hp('2.5%'),
    color: '#FFFFFF',
    letterSpacing: 1,
    paddingTop: hp('1.5%'),
    paddingLeft: wp('7.2%'),
    lineHeight: hp('2.8%'),
}
});
