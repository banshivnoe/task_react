import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducers from './reducers';
import {composeWithDevTools} from 'redux-devtools-extension';
import RootSagas from '../saga';
import {createLogger} from 'redux-logger';

let sagaMiddleware = createSagaMiddleware();

const bindMiddleware = () => {
    //apply logger only on dev environment to check redux prev and next state for an action
    if (__DEV__) {
        return applyMiddleware(sagaMiddleware, createLogger());
    }
    return applyMiddleware(sagaMiddleware);
};

const enhancer = composeWithDevTools({
    // Options: https://github.com/zalmoxisus/redux-devtools-extension/blob/master/docs/API/Arguments.md#options
})(bindMiddleware());

const configureStore = (preloadedState = {}) => {
    let store = createStore(reducers, preloadedState, enhancer);

    if (module.hot) {
        module.hot.accept(() => {
            store.replaceReducer(require('./reducers').default);
        });
    }
    sagaMiddleware.run(RootSagas);
    return store;
};

export default configureStore;
