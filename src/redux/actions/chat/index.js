import actionTypes from '../actionTypes';

export const chatMessageList = (group_id) => ({
    type: actionTypes.CHAT_LIST,
    group_id
});

export const chatMessageListSuccess = (payload) => {
    return {
        type: actionTypes.CHAT_LIST_SUCCESS,
        payload,
    };
};

export const chatMessageListFailure = (error) => ({
    type: actionTypes.CHAT_LIST_FAILURE,
    error: error,
});