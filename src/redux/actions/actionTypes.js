import mapKeys from 'lodash/mapKeys';

export default mapKeys([
    'CONTACT_LIST',
    'CONTACT_LIST_SUCCESS',
    'CONTACT_LIST_FAILURE',
    'CHAT_LIST',
    'CHAT_LIST_SUCCESS',
    'CHAT_LIST_FAILURE',
]);