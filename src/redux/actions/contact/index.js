import actionTypes from '../actionTypes';

export const contactUserList = (user_Id) => ({
    type: actionTypes.CONTACT_LIST,
    user_Id, 
});

export const contactUserListSuccess = (payload) => {
    return {
        type: actionTypes.CONTACT_LIST_SUCCESS,
        payload,
    };
};

export const contactUserListFailure = (error) => ({
    type: actionTypes.CONTACT_LIST_FAILURE,
    error: error,
});