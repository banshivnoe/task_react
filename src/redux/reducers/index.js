import {combineReducers} from 'redux';
import chat from './chat';
import contact from './contact';


const reducer = combineReducers({
    chat,
    contact,
});

export default reducer;