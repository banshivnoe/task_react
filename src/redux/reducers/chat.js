import actionTypes from '../actions/actionTypes';

const chatInitialState = {
  loading: false,
  error: {},
  chatMessage: {},

};

const chatReducer = (state = chatInitialState, {type, payload, error}) => {
  switch (type) {
      case actionTypes.CHAT_LIST_SUCCESS:
            const {data} = payload;
            return {
                ...state,
                loading: false,
                error: {},
                chatMessage: {
                    ...state.user,
                    ...(data.user || {}),
                },
            };
    case actionTypes.CHAT_LIST__FAILURE:
      return {
        ...state,
        loading: false,
        error,
      };

    default:
      return state;
  }
};

export default chatReducer;
