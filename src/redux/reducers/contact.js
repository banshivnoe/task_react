import actionTypes from '../actions/actionTypes';

const contactInitialState = {
  loading: false,
  error: {},
  userList: [],
};

const contactReducer = (state = contactInitialState, {type, payload, error}) => {
 
  switch (type) {
      case actionTypes.CONTACT_LIST_SUCCESS:
        //console.log("payload user reducer==>",payload.userChatList)
            return {
                ...state,
                loading: false,
                error: {},
                userList:payload.userChatList,
            };
    case actionTypes.CONTACT_LIST__FAILURE:
      return {
        ...state,
        loading: false,
        error,
      };

    default:
      return state;
  }
};

export default contactReducer;
