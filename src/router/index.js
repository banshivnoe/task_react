import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Contact from '../screens/Contact';
import Chat from '../screens/Chat';

const StackNavigator = createStackNavigator(); 
export const AppRouter = () => {
  return (
    <StackNavigator.Navigator initialRouteName="Contact">
              <StackNavigator.Screen name="Contact" component={Contact} />
             <StackNavigator.Screen name='Chat' component={Chat} />
    </StackNavigator.Navigator>
  );
};

