import React from 'react';
import {
  View,
  Text,
  Image,ImageBackground
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import DropShadow from 'react-native-drop-shadow';


export default  FavUserRender = ({index,item}) => {
    console.log("item =>",item.date)
    return (
      <DropShadow
        style={{
          shadowColor: 'rgba(0, 0, 0, 0.45)',
          marginLeft: wp('1.8%'),
          marginRight: wp('1.8%'),
          shadowOffset: {
            width: 0,
            height: 0,
          },
          shadowOpacity: 1,
          shadowRadius: 5,
        }}>
        <LinearGradient
          colors={['rgba(41, 47, 63, 0)', 'rgba(41, 47, 63, 0.8)']}
          style={{
            width: wp('26%'),
            height: hp('18%'),
            borderRadius: wp('7%'),
            marginTop: hp('2%'),
          }}>
          <ImageBackground
            source={{uri: item.user_profile}}
            imageStyle={{borderRadius: wp('8%')}}
            style={{
              width: wp('26%'),
              height: hp('18%'),
              resizeMode: 'cover',
              borderRadius: wp('8%'),
            }}>
            <View
              style={{
                flexDirection: 'column',
                position: 'absolute',
                bottom: hp('4%'),
                width: wp('26%'),
              }}>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontSize: hp('1.5%'),
                    lineHeight: hp('1.8%'),
                    letterSpacing: 1,
                    textAlign: 'left',
                    paddingLeft: wp('2.6%'),
                    paddingRight: wp('2.6%'),
                  }}>
                  {item.user_name}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Image
                  source={require('../assets/images/Union.png')}
                  style={{
                    height: wp('5%'),
                    width: wp('5%'),
                    position: 'absolute',
                    right: wp('3%'),
                  }}
                />
              </View>
            </View>
          </ImageBackground>
        </LinearGradient>
      </DropShadow>
    );
  };