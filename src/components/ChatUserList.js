import React from 'react';
import {
  View,
  Text,
  Image,
} from 'react-native';

import styles from '../styles';

export default ChatUsers = ({item}) => {
    const {user_name, user_profile, last_message, date, unSeenCount} = item;
    return (
      <View style={styles.chatRowContainer}>
        <View style={styles.ImageProfleContainer}>
          <Image
            source={{uri: user_profile}}
            style={
              unSeenCount !== '0'
                ? styles.profilePic
                : styles.profilePicWithoutCount
            }
          />
          {unSeenCount !== '0' && (
            <View style={styles.viewCountContainer}>
              <Text style={styles.textCount}>+{unSeenCount}</Text>
            </View>
          )}
        </View>
  
        <View style={styles.userMessageContainer}>
          <Text style={styles.userNameText}>{user_name}</Text>
          <Text style={styles.userMessageText}>{last_message}</Text>
        </View>
  
        <View style={styles.dateTimeComtainer}>
          <Text style={styles.dateTimeText}>{date}</Text>
        </View>
      </View>
    );
  };