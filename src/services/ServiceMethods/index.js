import axios from '../instance';

import {
    CONTACT_LIST,
    CHAT_MESSAGE,
} from '../api-config';

export const contactListApi = (params) => axios.post(CONTACT_LIST, params);
export const chatListApi = (params) => axios.post(CHAT_MESSAGE, params);
