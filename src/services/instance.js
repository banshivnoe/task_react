import axios from 'axios';
//create an axios instance with default headers and other necessary configs which needs to be attached inside headers
const instance = axios.create({
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
});

//use Interceptors request for preprocessing a request.
instance.interceptors.request.use(
    async (config) => {
       // we can add custom header param
        return config;
    },
    (error) => Promise.reject(error),
);

//use Interceptors response for postprocessing a request (generally on error handling).
instance.interceptors.response.use(
    (response) => {
        return response;
    },
    async function (error) {
        const originalRequest = error.config;
        if (
            ((error || {}).response || {}).status === 401 &&
            !originalRequest._retry
        ) {
            originalRequest._retry = true;
           // we can build here some session manage
           return axios(originalRequest);
        }

        // return Error object with Promise
        return Promise.reject(error);
    },
);
export default instance;
