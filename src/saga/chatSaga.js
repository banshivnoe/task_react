import {takeLatest, put, call} from 'redux-saga/effects';
import actionTypes from '../redux/actions/actionTypes';
import {chatListApi} from '../services/ServiceMethods';

import {
  chatMessageListSuccess,
  chatMessageListFailure
} from '../redux/actions/chat';

function* chatList({user_id}) {
  try {
    // yield call takes the function as an arg followed by the arguments to pass to the function: https://redux-saga.js.org/docs/api/#callfn-args.

    const response = yield call(chatListApi, {
      user_id
    });

    const dataResponse = {
    };
    yield put(chatMessageListSuccess(dataResponse));

    if (response.data && response.data.status === 'success') {
      yield put(chatMessageListSuccess(response.data));
    } else {
      yield put(chatMessageListFailure(response.data));
    }
  } catch (error) {
    yield put(chatMessageListFailure(error.response || error));
  }
}

export function* watchChatList() {
  yield takeLatest(actionTypes.CHAT_LIST, chatList);
}

