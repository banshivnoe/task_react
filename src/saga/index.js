import {all} from 'redux-saga/effects';
import {
    watchChatList,
} from './chatSaga.js';

import {
    watchContactList,
} from './contactSaga.js';

const rootSaga = function* () {
    yield all([
        /*add generator function here*/
        watchChatList(),
        watchContactList(),   
    ]);
};

export default rootSaga;
