import {takeLatest, put, call} from 'redux-saga/effects';
import actionTypes from '../redux/actions/actionTypes';
import {contactListApi} from '../services/ServiceMethods';

import {
  contactUserListFailure,
  contactUserListSuccess,
} from '../redux/actions/contact';

function* contactlist({user_Id}) {
  try {
 
 
    console.log("user_Id saga ==>",user_Id);

    // yield call takes the function as an arg followed by the arguments to pass to the function: https://redux-saga.js.org/docs/api/#callfn-args.

    //comment for service call with endpoints
    // const response = yield call(contactListApi, {
    //     user_Id
    // });

    const response = {error:null,status:'sucess',userChatList: [
      {
        user_id: 0,
        user_name: 'Bozenka Malina',
        user_profile:
          'https://s3-alpha-sig.figma.com/img/48e5/98ff/b83245fdcf42dfe0edb37a51fe8909e6?Expires=1625443200&Signature=AetasLWyChw35gA5vqabyb~vy~7ruR4YwXJH8R~3EjoBz9eetsXZ4LX~yoMp4p03P2RCTjLGfinc4jP8lI1YfjD2q5C9vKmEJjwevhzwisgYM3pYh81L2QfHkJfiXslFaVSQrGXe6KXvNUhSZfMnMkuyA7LWr6PAP2z~e4RprY3XojXTd7TdjG4LrErk1xvM56KrtWG6vdKX1uQdtK6ehve3rYOSfiD-89m-tXkdZdY94psesCLD22lb1KE-J2CFWrfZ0jeLlHDtCs6P5NQ45BpD9k-I9ox0sD1Q9CZEoZR9gsBKEK4HYTvvAQVB9Wn1q3WjEArk9tMvjJw7w8qhtA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
        last_message: 'uploaded file',
        date: 'Sun',
        unSeenCount: '5',
        isFav: '0',
      },
      {
        user_id: 1,
        user_name: 'Odeusz Piotrowski',
        user_profile:
          'https://s3-alpha-sig.figma.com/img/9923/16fd/b43d07e5e5a00a8fff44a02a500d7846?Expires=1625443200&Signature=FjuBZEZYsJ6xASdgPmJn1FqS6JGeGc8wPqA7QzhMGxpMja3pIyizWM7RhRQzjIXGh3Dc01QqyOvp-xIEwUEHsv3tWNrKqXuyxRJ83xkA~zVGBr4Ty6H~MQ0oFjyEyMc1cg8~1LFHhgJaL~W9F4ykOXUUbngm3Wi62IWK-~e9vJaQ5Ki2tFQ5WRcZ3wn~Q21tsPkyx3ynUCbBboDFqVInzIqEhMVw3yxNWV4Y6sq-3WRf96eFxDO2ua3DWJ2K20Fh2pkN0cWFapDy~iUZNIzBPJtu~wuzV6YgyZT4rP5JVn6wfTfaFnP12SiNQIU28djYM-Da7alrzh6PoIWaJDwOLQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
        last_message: 'will do, super, thank you',
        date: '01 Feb',
        unSeenCount: '22',
        isFav: '1',
      },
      {
        user_id: 2,
        user_name: 'jaroslaw kowalski',
        user_profile:
          'https://s3-alpha-sig.figma.com/img/a9d6/ab5b/d06170e991dc7aa0eb91e9569e03b38f?Expires=1625443200&Signature=GakI7vi2j3g5oGzIEFxfIjKq1uASkql6yd-b-90UAPHn~QTIwlcHEaNV1-GeWmJrG3lBI32fxDVwXA6nKD~mWRh~rwKsrXxXsfV9U5-pkDhtUDsnrCbF-IUTboFOvjhMH0V9Ngo-pAcgretVwgtpg9~5f5BpC9ApGCnhShucEVWCvWPVeh2j47ohpNn~MUiSRvxoL0oZFiURh6fTa~JmMEzMPVTkWzwdVyROcu95G3zX-hIleJ-Rq0jdAM7FDkpVB2ERGVsJC6w1qTr4qdcZYwQEmtqTuZ7bosgil8UFvyB4zYvSSr8kuNa-OWF-hSWTg00~oh9TduE5FCVJjBxT~A__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
        last_message: 'how is coronavirus',
        date: 'Mon',
        unSeenCount: '0',
        isFav: '1',
      },
      {
        user_id: 3,
        user_name: 'Bozenka Malina',
        user_profile:
          'https://s3-alpha-sig.figma.com/img/48e5/98ff/b83245fdcf42dfe0edb37a51fe8909e6?Expires=1625443200&Signature=AetasLWyChw35gA5vqabyb~vy~7ruR4YwXJH8R~3EjoBz9eetsXZ4LX~yoMp4p03P2RCTjLGfinc4jP8lI1YfjD2q5C9vKmEJjwevhzwisgYM3pYh81L2QfHkJfiXslFaVSQrGXe6KXvNUhSZfMnMkuyA7LWr6PAP2z~e4RprY3XojXTd7TdjG4LrErk1xvM56KrtWG6vdKX1uQdtK6ehve3rYOSfiD-89m-tXkdZdY94psesCLD22lb1KE-J2CFWrfZ0jeLlHDtCs6P5NQ45BpD9k-I9ox0sD1Q9CZEoZR9gsBKEK4HYTvvAQVB9Wn1q3WjEArk9tMvjJw7w8qhtA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
        last_message: '😂',
        date: 'Sun',
        unSeenCount: '5',
        isFav: '0',
      },
      {
        user_id: 4,
        user_name: 'Odeusz Piotrowski',
        user_profile:
          'https://s3-alpha-sig.figma.com/img/049e/630c/4f71f5dabd2806cdf2419c05b72f278f?Expires=1625443200&Signature=EgYTGAeaPadYZwrU26gJ3TrJclOFNjeAksOdPE-K6MX6w~ZFjqF6toMXxKcbFwN7jOon-cFZdhy7aByx76Q2f-VNzo4TF~M-eerjobLaxla1egB981zIwC5ebp6BuSzb4jM6j5qlLyhKppHYufFj809qAuTvjlJI0jsxLx7L05RnyE3h3iSqjp1mhLvy77IilVsrASkx~FO-dTufERTAkHbilmC851CY6vTzYCCm37UJOAZ0NEHbrYbIYXttmZpIc4p2L0HxUOIMeC6EXRZeYOXoUhFlvjK-D6~~yA1SMqdmuLJiU5Jbgrr-RshX72Dcv46uVMfLFJJ0iuHYhxaOSA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
        last_message: 'will do, super, thank you',
        date: '18 Mar',
        unSeenCount: '22',
        isFav: '1',
      },
      {
        user_id: 5,
        user_name: 'jaroslaw kowalski',
        user_profile:
          'https://s3-alpha-sig.figma.com/img/a9d6/ab5b/d06170e991dc7aa0eb91e9569e03b38f?Expires=1625443200&Signature=GakI7vi2j3g5oGzIEFxfIjKq1uASkql6yd-b-90UAPHn~QTIwlcHEaNV1-GeWmJrG3lBI32fxDVwXA6nKD~mWRh~rwKsrXxXsfV9U5-pkDhtUDsnrCbF-IUTboFOvjhMH0V9Ngo-pAcgretVwgtpg9~5f5BpC9ApGCnhShucEVWCvWPVeh2j47ohpNn~MUiSRvxoL0oZFiURh6fTa~JmMEzMPVTkWzwdVyROcu95G3zX-hIleJ-Rq0jdAM7FDkpVB2ERGVsJC6w1qTr4qdcZYwQEmtqTuZ7bosgil8UFvyB4zYvSSr8kuNa-OWF-hSWTg00~oh9TduE5FCVJjBxT~A__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA',
        last_message: 'how is coronavirus',
        date: 'Mon',
        unSeenCount: '0',
        isFav: '1',
      },
    ]};

    yield put(contactUserListSuccess(response));
    //condintion comment for without endopoints
    // if (response.data && response.data.status === 'success') {
    //   yield put(contactUserListSuccess(response.data));
    // } else {
    //   yield put(contactUserListFailure(response.data));
    // }
  } catch (error) {
    yield put(contactUserListFailure(error.response || error));
  }
}

export function* watchContactList() {
  yield takeLatest(actionTypes.CONTACT_LIST, contactlist);
}
