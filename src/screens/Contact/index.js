import React, {useEffect} from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  TextInput,
} from 'react-native';
import GeneralStatusBarColor from '../../helper/GeneralStatusBarColor';
import styles from '../../styles';
import {colors} from '../../utils/colors';
import {
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
import Dots from 'react-native-dots-pagination';

import FavUserRender from '../../components/CrousalComponent'
import ChatUsers from '../../components/ChatUserList'
import {contactUserList} from '../../redux/actions/contact'

const ItemSeparator = () => <View style={styles.lineSeprator} />;

const Contact = () => {

  const {userList} = useSelector(state => state.contact);
  
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(contactUserList('1'))
}, []);

console.log("User list in contact==>",userList);
  return (
    <View style={styles.mainContainer}>
      <GeneralStatusBarColor
        backgroundColor={colors.themeColor}
        barStyle="light-content"
      />
      <View style={styles.rowHeader}>
        <View style={styles.headerImage}>
          <Image
            source={{uri: userList!=0?userList[2].user_profile:''}}
            style={styles.profilePicWithoutCount}
          />
        </View>
        <View
          style={styles.headerNameCont}>
          <Text
            style={styles.userNameTitle}>
            Martina Wolna
          </Text>
        </View>
      </View>
      <View style={styles.rowSearch}>
        <View
          style={styles.searchContainer}>
          <View
            style={styles.searchView}>
            <View style={{flexDirection: 'column'}}>
              <TextInput
                style={styles.searchBarText}
                placeholder="Search..."
                underlineColorAndroid='transparent'
                placeholderTextColor = "rgba(255, 255, 255, 0.6)"
              />
            </View>
            <View
              style={styles.searchBarImage}>
              <View
                style={styles.searchBarIconBack}>
                <Image
                  source={require('../../assets/images/Subtract.png')}
                  style={styles.searchBarIcon}
                />
              </View>
            </View>
          </View>
        </View>
        <View
          style={styles.buttonPlusContainer}>
          <View
            style={styles.buttonPlusIcon}>
            <Text
              style={styles.textPlus}>
              {' '}
              +{' '}
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.rowUsers}>
        <View style={{flexDirection: 'column'}}>
          <Text
            style={styles.textFav}>
            Favourites
          </Text>
          <FlatList
            data={userList}
            horizontal
            keyExtractor={item => item.user_id}
            showsHorizontalScrollIndicator={false}
            renderItem={(item)=>{
              return(<FavUserRender item={item.item}/>)
            }}
          />
          <Dots
            length={userList.length}
            active={1}
            activeColor={'rgba(196, 196, 196, 1)'}
            passiveColor={'rgba(196, 196, 196, 0.4)'}
            passiveDotWidth={wp('1.5%')}
            passiveDotHeight={wp('1.5%')}
            activeDotHeight={wp('1.5%')}
            activeDotWidth={wp('1.5%')}
            marginHorizontal={10}
          />
        </View>
      </View>
      <View style={styles.rowChatList}>
        <FlatList
          data={userList}
          renderItem={(item)=>{
            return(<ChatUsers item={item.item}/>)
          }}
          ItemSeparatorComponent={ItemSeparator}
          keyExtractor={item => item.user_id}
        />
      </View>
    </View>
  );
};

export default Contact;
